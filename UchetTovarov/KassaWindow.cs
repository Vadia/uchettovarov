﻿using System;
using Npgsql;
using System.Data.Common;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


namespace UchetTovarov
{
    public partial class KassaWindow : Form
    {
        public KassaWindow()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            mainwindow.checkclosewindow = 1;
            this.Close();
        }
        int summ = 0;
        int i = 0;
        private void KSK_FormClosed(object sender, FormClosedEventArgs e)
        {
            String connectionString = "Server=localhost;Port=5432;User Id=postgres;Password=020225;Database=UchetTov;";

            NpgsqlConnection npgSqlConnection = new NpgsqlConnection(connectionString);
            npgSqlConnection.Open();

            NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT * FROM tovar WHERE article = '{textBox1.Text}'", npgSqlConnection);
            NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();
            if (npgSqlDataReader.HasRows)
            {  
                foreach (DbDataRecord dbDataRecord in npgSqlDataReader)
                {
                    dataGridView1.Rows.Add(i,"" + dbDataRecord["article"], "" + dbDataRecord["name"], "" + KassaSelectKolvo.Kolvo, "" + dbDataRecord["secondprice"]);
                    summ = summ + (Convert.ToInt32(dbDataRecord["secondprice"])* KassaSelectKolvo.Kolvo);
                    label2.Text = Convert.ToString(summ);
                }
            }
            npgSqlConnection.Close();
            i++;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            KassaSelectKolvo KSK = new KassaSelectKolvo();
            KSK.FormClosed += new FormClosedEventHandler(KSK_FormClosed);
            KSK.Show();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            int ind = dataGridView1.SelectedCells[0].RowIndex;

            int q = Convert.ToInt32(dataGridView1[3, ind].Value);
            int w = Convert.ToInt32(dataGridView1[4, ind].Value);

            summ = summ - (q * w);
            label2.Text = Convert.ToString(summ);

            dataGridView1.Rows.RemoveAt(ind);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            String spos = "";
            String positions = "";
            if (checkBox1.Checked == true) {
                spos = "Наличный расчет";
            } else if (checkBox2.Checked == true) {
                spos = "Безналичный расчет";
            }

            //Списание с склада
            String connectionString = "Server=localhost;Port=5432;User Id=postgres;Password=020225;Database=UchetTov;";

            for (int i = 0; i < dataGridView1.RowCount - 1; i++)
            {
                int kolvofromBD = 0;

                positions += dataGridView1[1, i].Value.ToString() + "," + dataGridView1[2, i].Value.ToString() + "," + dataGridView1[3, i].Value.ToString() + "," + dataGridView1[4, i].Value.ToString()+ ". ";
                
                NpgsqlConnection npgSqlConnection = new NpgsqlConnection(connectionString);
                npgSqlConnection.Open();

                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT * FROM tovar WHERE article = '{dataGridView1[1, i].Value.ToString()}'", npgSqlConnection);
                NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();
                if (npgSqlDataReader.HasRows)
                {
                    foreach (DbDataRecord dbDataRecord in npgSqlDataReader)
                    {
                        kolvofromBD = Convert.ToInt32(dbDataRecord["kolvo"]);
                    }
                }
                npgSqlConnection.Close();



                kolvofromBD = kolvofromBD - Convert.ToInt32(dataGridView1[3, i].Value);


                NpgsqlConnection npgSqlConnection1 = new NpgsqlConnection(connectionString);
                npgSqlConnection1.Open();
                NpgsqlCommand npgSqlSpisanie = new NpgsqlCommand($"UPDATE tovar SET kolvo = '{kolvofromBD}' WHERE article = '{dataGridView1[1, i].Value.ToString()}'", npgSqlConnection1);
                int result = npgSqlSpisanie.ExecuteNonQuery();
                npgSqlConnection1.Close();


            }
            //запись в таб продаж
            NpgsqlConnection npgSqlConnection2 = new NpgsqlConnection(connectionString);
            npgSqlConnection2.Open();
            
            NpgsqlCommand npgSqlADD = new NpgsqlCommand($"INSERT INTO sales(kassa, sum, sposob, positions) VALUES ('{mainwindow.ActiveLogin}', '{label2.Text}', '{spos}', '{positions}')", npgSqlConnection2);
            int count11 = npgSqlADD.ExecuteNonQuery();
            
            npgSqlConnection2.Close();

            textBox1.Text = "Артикул";
            label2.Text = "Сумма";
            dataGridView1.Rows.Clear();
            checkBox1.Checked = false;
            checkBox2.Checked = false;


            MessageBox.Show("Успех!");
        }

        private void KassaWindow_Load(object sender, EventArgs e)
        {

        }
    }
}
