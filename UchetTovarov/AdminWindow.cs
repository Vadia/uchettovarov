﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace UchetTovarov
{
    public partial class AdminWindow : Form
    {
        public AdminWindow()
        {
            InitializeComponent();
        }

        public static int checkclosewindow = 0;
        private void AdminWindow_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddNewUser ANU = new AddNewUser();
            ANU.FormClosed += new FormClosedEventHandler(ANU_FormClosed);
            ANU.Show();
            
        }

        private void ANU_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (checkclosewindow == 0)
            {
                this.Close();
            }

        }

        private void ART_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (checkclosewindow == 0)
            {
                this.Close();
            }

        }




        private void button2_Click(object sender, EventArgs e)
        {
            mainwindow.checkclosewindow = 1;
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AdminRedTovar ART = new AdminRedTovar();
            ART.FormClosed += new FormClosedEventHandler(ART_FormClosed);
            ART.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            AdminUsersList AUL = new AdminUsersList();
            AUL.Show();
        }
    }
}
