﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UchetTovarov
{
    public partial class TovarovedWindow : Form
    {
        public TovarovedWindow()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TovarovedPriemTov TPT = new TovarovedPriemTov();
            TPT.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            mainwindow.checkclosewindow = 1;
            this.Close();
        }
    }
}
