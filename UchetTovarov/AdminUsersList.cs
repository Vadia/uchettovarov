﻿using System;
using Npgsql;
using System.Data.Common;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UchetTovarov
{
    public partial class AdminUsersList : Form
    {
        public AdminUsersList()
        {
            InitializeComponent();

            //подключение к бд и получение данных
            String connectionString = "Server=localhost;Port=5432;User Id=postgres;Password=020225;Database=UchetTov;";

            NpgsqlConnection npgSqlConnection = new NpgsqlConnection(connectionString);
            npgSqlConnection.Open();
            Console.WriteLine("Соединение с БД открыто");
            NpgsqlCommand npgSqlCommand = new NpgsqlCommand("SELECT * FROM Users", npgSqlConnection);
            NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();
            if (npgSqlDataReader.HasRows)
            {
                foreach (DbDataRecord dbDataRecord in npgSqlDataReader)
                {
                    dataGridView1.Rows.Add("" + dbDataRecord["id"], "" + dbDataRecord["login"], "" + dbDataRecord["email"], "" + dbDataRecord["grp"]);
                }
            }
            else
                npgSqlConnection.Close();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AdminDelUser ADU = new AdminDelUser();
            ADU.Show();
        }
    }
}
