﻿using System;
using Npgsql;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Common;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace UchetTovarov
{
    public partial class mainwindow : Form
    {


        public mainwindow()
        {
            InitializeComponent();
            
        }

        public static int checkclosewindow = 0;
        public static string ActiveLogin = "";

        private void button1_Click(object sender, EventArgs e)
        {

            String login ="";
            String pass = "";
            String grp = "";
            
            //подключение к бд и получение данных
            String connectionString = "Server=localhost;Port=5432;User Id=postgres;Password=020225;Database=UchetTov;";
            
            NpgsqlConnection npgSqlConnection = new NpgsqlConnection(connectionString);
            npgSqlConnection.Open();
            Console.WriteLine("Соединение с БД открыто");
            NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT * FROM Users WHERE login = '{textBox1.Text}'", npgSqlConnection);
            NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();
            if (npgSqlDataReader.HasRows)
            {
                foreach (DbDataRecord dbDataRecord in npgSqlDataReader)
                {

                    ActiveLogin = "" + dbDataRecord["login"];
                    login = ""+dbDataRecord["login"];
                    pass = "" + dbDataRecord["pass"];
                    grp = ""+dbDataRecord["grp"];
                }
            }
            
            npgSqlConnection.Close();
            label2.Text = "Пользователь не найден!";

            if (login.Equals(textBox1.Text) && pass.Equals(textBox2.Text))
            {


                if (grp.Equals("1"))//Админ
                {
                    label2.Text = "";
                    AdminWindow f1 = new AdminWindow();
                    f1.FormClosed += new FormClosedEventHandler(f1_FormClosed);
                    f1.Show();
                    this.Hide();
                }

                if (grp.Equals("2"))//Касса
                {
                    label2.Text = "";
                    KassaWindow f2 = new KassaWindow();
                    f2.FormClosed += new FormClosedEventHandler(f2_FormClosed);
                    f2.Show();
                    this.Hide();
                }

                if (grp.Equals("3")) //Товаровед
                {
                    label2.Text = "";
                    TovarovedWindow f3 = new TovarovedWindow();
                    f3.FormClosed += new FormClosedEventHandler(f3_FormClosed);
                    f3.Show();
                    this.Hide();
                }

            }
            else {
                label2.Text = "Логин или Пароль не верны!";
            }

        }


        private void f1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (checkclosewindow == 0)
            {
                this.Close();
            }
            else {
                this.Show();
            }
        }
        private void f2_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (checkclosewindow == 0)
            {
                this.Close();
            }
            else
            {
                this.Show();
            }
        }
        private void f3_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (checkclosewindow == 0)
            {
                this.Close();
            }
            else
            {
                this.Show();
            }
        }

        private void mainwindow_Load(object sender, EventArgs e)
        {

        }
    }

    
}
