﻿using System;
using Npgsql;
using System.Data.Common;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UchetTovarov
{
    public partial class AddNewUser : Form
    {
        public AddNewUser()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String grp = (string)comboBox1.SelectedItem;
            String grpS = grp.Substring(0, 1);


            String connectionString = "Server=localhost;Port=5432;User Id=postgres;Password=020225;Database=UchetTov;";

            NpgsqlConnection npgSqlConnection = new NpgsqlConnection(connectionString);
            npgSqlConnection.Open();

            NpgsqlCommand npgSqlCheckLogin = new NpgsqlCommand($"SELECT * FROM users WHERE login = '{textBox1.Text}';", npgSqlConnection);
            NpgsqlDataReader npgSqlDataReaderCheckLogin = npgSqlCheckLogin.ExecuteReader();
            if (npgSqlDataReaderCheckLogin.HasRows)
            {
                label4.Text = "Никнейм занят!";
                npgSqlConnection.Close();
            }
            else {
                npgSqlConnection.Close();


                NpgsqlConnection npgSqlConnection1 = new NpgsqlConnection(connectionString);
                npgSqlConnection1.Open();

                NpgsqlCommand npgSqlIns = new NpgsqlCommand($"INSERT INTO users(login, pass, email, grp) VALUES ('{textBox1.Text}', '{textBox2.Text}','{textBox3.Text}' ,'{grpS}' );", npgSqlConnection1);
                int count = npgSqlIns.ExecuteNonQuery();
                if (count == 1)
                {

                    label4.Text = "Успех!";
                }
                else
                {

                    label4.Text = "Неудача!";
                }
                npgSqlConnection1.Close();
            }



        }

        private void button2_Click(object sender, EventArgs e)
        {
            AdminWindow.checkclosewindow = 1;
            this.Close();
        }

        private void AddNewUser_Load(object sender, EventArgs e)
        {

        }
    }
}
